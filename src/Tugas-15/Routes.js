import React from "react";
import { Switch, Route } from "react-router-dom";

import Tugas9 from '../Tugas-9/FormPembelianBuah';
import Tugas10 from '../Tugas-10/TabelHargaBuah';
import Tugas11 from '../Tugas-11/Clock';
import Tugas12 from '../Tugas-12/Lists';
import Tugas13 from '../Tugas-13/HooksLists';
import Tugas14 from '../Tugas-14/Buah';
import Tugas15 from './SwitchTheme';
import Nav from './Nav';
import { ThemeProvider } from "./ThemeContext";

export default function App() {
  return (
      <>
        <ThemeProvider>          
          <Nav/>
          <Switch>
            <Route exact path="/">
              <Tugas9 />
            </Route>

            <Route exact path="/tugas10">
              <Tugas10 />
            </Route>

            <Route exact path="/tugas11">
              <Tugas11 start={200} />
            </Route>

            <Route path="/tugas12">
              <Tugas12 />
            </Route>
            
            <Route path="/tugas13">
              <Tugas13 />
            </Route>

            <Route path="/tugas14">
              <Tugas14 />
            </Route>

            <Route path="/tugas15">
              <Tugas15 />
            </Route>
          
          </Switch>
        </ThemeProvider>
      </>
  );
}