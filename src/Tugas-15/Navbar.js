import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import { ThemeContext } from './ThemeContext';


export default function NavigationBar() {
    const state = useContext(ThemeContext)

    return (
        <nav>
          <ul>
            <li>
              <Link to="/">Tugas 9</Link>
            </li>
            <li>
              <Link  to="/Tugas10">Tugas 10</Link>
            </li>
            <li>
              <Link to="/Tugas11">Tugas 11</Link>
            </li>
            <li>
              <Link to="/Tugas12">Tugas 12</Link>
            </li>
            <li>
              <Link to="/Tugas13">Tugas 13</Link>
            </li>
            <li>
              <Link to="/Tugas14">Tugas 14</Link>
            </li>
            <li>
              <Link to="/Tugas15">Tugas 15</Link>
            </li>
          </ul>
        </nav>
    )
    }