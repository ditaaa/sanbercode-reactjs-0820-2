  
import React, {useContext} from 'react';
import ThemeContext from './ThemeContext'

const Layout = () => {
    const theme = useContext(ThemeContext)
    return(
        <div style={theme}>
        {JSON.stringify(theme)}
        </div>
    )
}

export default Layout;