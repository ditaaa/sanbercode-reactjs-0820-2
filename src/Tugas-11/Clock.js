import React, { Component } from "react";


class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      timer: 100,
      visible: true
    };
  }

  componentDidMount() {
    this.clock = setInterval(() => this.tick(), 1000);
    this.timer = setInterval(
      () => this.setState((prevState) => ({ timer: prevState.timer - 1 })),
      1000
    );
  }

  componentDidUpdate() {
    if(this.state.visible === true) {
        if(this.state.timer === 0) {
            this.setState({
                visible: false
            })
        }
    }
  }

  tick() {
    this.setState({
      date: new Date(),
    });
  }

  render() {
    return (
        <div>
        {this.state.visible === true && (
            
            <div style={{ textAlign: "center", display: "flex", "justify-content": "space-between"}}>
              <h1>sekarang jam : {this.state.date.toLocaleTimeString('en-US')}</h1>
              <h1>hitung mundur : {this.state.timer}</h1>
            </div>
        )}
        </div>
    );
  }
}


export default Clock;