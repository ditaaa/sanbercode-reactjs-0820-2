import React, { useState, useEffect } from "react";
import axios from "axios";

const HooksLists = () =>{
  const [dataHargaBuah, setBuah] = useState(null);
  const [input, setInput] = useState({
    name: "",
    price: "",
    weight: 0,
    id: null,
  });

  useEffect(() =>{
    if (dataHargaBuah === null) {
      axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
        .then((res) => {
          const buah = res.data;
          setBuah(buah);
        });
    }
  }, [dataHargaBuah]);

  const submitForm = (event) => {
    event.preventDefault();
    if (input.id === null) {
      axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {
          name: input.name,
          price: input.price,
          weight: input.weight,
        })
        .then((res) => {
          let data = res.data;
          setBuah([...dataHargaBuah,
            {
              name: data.name,
              price: data.price,
              weight: data.weight,
              id: data.id,
            },
          ]);
          setInput({
            name: "",
            price: "",
            weight: 0,
            id: null,
          });
        });

    } else {
      axios.put(`http://backendexample.sanbercloud.com/api/fruits/${input.id}`, {
          name: input.name,
          weight: input.weight,
          price: input.price,
        })
        .then((res) => {
          var newDataBuah = dataHargaBuah.map((x) =>{
            if (x.id === input.id) {
              x.name = input.name;
              x.price = input.price;
              x.weight = input.weight;
            }
            return x;
          });
          setBuah(newDataBuah);
          setInput({
            name: "",
            price: "",
            weight: 0,
            id: null,
          });
        });
    }
  };

  const myChangeHandle = (event) => {
    let nam = event.target.name;
    var value = event.target.value;
    console.log(value);
    setInput({ ...input, [nam]: value });
  };

  const editForm = (event) => {
    const idBuah = parseInt(event.target.value);
    console.log(idBuah);
    var buah = dataHargaBuah.find((x) => x.id === idBuah);
    setInput({
      id: idBuah,
      name: buah.name,
      price: buah.price,
      weight: buah.weight,
    });
  };

  const deleteForm = (e) => {
    var idBuah = parseInt(e.target.value);
    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
      .then((res) => {
        var newDataBuah = dataHargaBuah.filter((x) => x.id !== idBuah);
        setBuah(newDataBuah);
      });
  };

  return (
    <div>
      {/* Tabel Buah*/}
      <h1 style={{textAlign: 'center'}}>Tabel Harga Buah</h1>
      <table className="tabelBuah">
        <thead>
          <tr>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          {dataHargaBuah !== null &&
            dataHargaBuah.map((el, index) => {
              return (
                <tr key={el.id}>
                  <td>{el.name}</td>
                  <td>{el.price}</td>
                  <td>{el.weight / 1000} kg</td>
                  <td>
                    <button value={el.id} style={{ marginRight: "10px" }} onClick={editForm}>Edit</button>
                    <button value={el.id} onClick={deleteForm}>Delete</button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>

      {/* Form Buah*/}
      <form onSubmit={submitForm}>
        <h1>Form Input Buah</h1>
        <label>Nama Buah :</label>
        <input require type="text"
          name="name"
          value={input.name}
          onChange={myChangeHandle}
        />
        <label>Harga Buah :</label>
        <input
          required
          type="text"
          name="price"
          value={input.price}
          onChange={myChangeHandle}
        />
        <label>Berat (gram) :</label>
        <input
          required
          type="number"
          name="weight"
          value={input.weight}
          onChange={myChangeHandle}
        />
        <button className="submit">Submit</button>
      </form>
    </div>
  );
};


export default HooksLists;