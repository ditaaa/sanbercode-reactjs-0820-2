import React from "react"
import {BuahProvider} from "./BuahContext"
import TabelBuah from "./TabelBuah"
import FormBuah from "./FormBuah"


const Buah = () =>{
  return(
    <BuahProvider>
        <TabelBuah/>
        <FormBuah/>
    </BuahProvider>
  )
}


export default Buah;