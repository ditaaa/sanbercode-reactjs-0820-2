import React, {Component} from "react"


class Lists extends Component{
    constructor(props) {
        super(props)
        this.state = {
            dataHargaBuah : [
                {nama: "Semangka", harga: 10000, berat: 1000},
                {nama: "Anggur", harga: 40000, berat: 500},
                {nama: "Strawberry", harga: 30000, berat: 400},
                {nama: "Jeruk", harga: 30000, berat: 1000},
                {nama: "Mangga", harga: 30000, berat: 500}
              ],
              inputNama: "",
              inputHarga: "",
              inputBerat: "",
              index: -1
        };
    }

    myChangeHandle = (event) =>{
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
    }

    submitForm = (event) =>{
        event.preventDefault()
        console.log(this.state.inputNama)
        console.log(this.state.inputHarga)
        console.log(this.state.inputBerat)
        let index = this.state.index

        if(index === -1){
            this.setState({
                dataHargaBuah: [...this.state.dataHargaBuah,
                {
                    nama: this.state.inputNama,
                    harga: this.state.inputHarga,
                    berat: this.state.inputBerat 
                }
            ],
            inputNama: "",
            inputHarga: "",
            inputBerat: ""
            })
        
        }else{
            var newDataHargaBuah = this.state.dataHargaBuah
            newDataHargaBuah[index] = {
                nama: this.state.inputNama,
                harga: this.state.inputHarga,
                berat: this.state.inputBerat,
            }
            this.setState({
                dataHargaBuah: [...newDataHargaBuah],
                inputNama : "",
                inputHarga : "",
                inputBerat : "",
                index : -1
        })
    }
}
    editForm = (event) =>{
        let index = event.target.value
        let namaBuah = this.state.dataHargaBuah[index].nama
        let hargaBuah = this.state.dataHargaBuah[index].harga
        let beratBuah = this.state.dataHargaBuah[index].berat
        
        this.setState({
            inputNama: namaBuah,
            inputHarga: hargaBuah,
            inputBerat: beratBuah,
            index
        })
    }

    deleteForm = (event) =>{
        let index = event.target.value;
        console.log(index)
        let removeobject = this.state.dataHargaBuah.splice(index, 1)
        console.log(removeobject)
        console.log(this.state.dataHargaBuah)
        this.setState({
            dataHargaBuah : this.state.dataHargaBuah,
            index: -1
        })
    }

      render(){
          return(
            <div style={{width: '100%', margin :'0 auto', border: '1px transparent', borderRadius: '15px', padding: '15px', fontFamily: 'times new roman'}}>
            <div>
                <h1 style={{textAlign : 'center',  marginBottom: '0.5em'}}>Tabel Harga Buah</h1>
            </div>
            <div>
                <table border = '2px solid #black' style={{margin: '0 auto', width:'70%', textAlign:'center',  marginBottom: '2em'}}>
                    <thead>
                    <tr style={{backgroundColor:'#9b9ea3'}}>
                        <th style={{textAlign : 'center'}}>Nama</th>
                        <th style={{textAlign : 'center'}}>Harga</th>
                        <th style={{textAlign : 'center'}}>Berat</th>
                        <th style={{textAlign : 'center'}}>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.state.dataHargaBuah.map((el,index) =>{
                        return(
                    <tr  style={{backgroundColor:'#f59149'}} key={index}>
                        <td>{el.nama}</td>
                        <td>{el.harga}</td>
                        <td>{el.berat/1000} Kg</td>
                        <td style={{textAlign:'center'}}>
                            <button value={index} style={{marginRight:'7px'}} onClick={this.editForm} >Edit</button>
                            <button value={index} onClick={this.deleteForm}>Delete</button></td>
                    </tr>
                        )
                    }
                )
            }
            </tbody>
            </table>

            <form onSubmit={this.submitForm} style={{textAlign : 'center', marginBottom: '0.5em'}}>
                <h1  style ={{marginBottom: '1em', textAlign : 'center'}}>Form Input Buah</h1>
                <label style={{textAlign : 'center'}}>Nama Buah :</label> <br/>
                <input style={{margin: '0 auto', width:'70%', textAlign:'center'}}
                required type='text' name="inputNama" value={this.state.inputNama} onChange={this.myChangeHandle}></input> <br/> <br/>
                <label style={{textAlign : 'center'}}>Harga Buah :</label> <br/>
                <input style={{margin: '0 auto', width:'70%', textAlign:'center'}} 
                required type='text' name="inputHarga" value={this.state.inputHarga} onChange={this.myChangeHandle}></input> <br/> <br/>
                <label style={{textAlign : 'center'}}>Berat (gram) :</label> <br/>
                <input style={{margin: '0 auto', width:'70%', textAlign:'center'}}
                required type='text' name="inputBerat" value={this.state.inputBerat} onChange={this.myChangeHandle}></input> <br/> <br/>
            
            <button style={{marginTop: '15px', backgroundColor:'#9b9ea3', textAlign : 'center'}} >Submit</button> <br/> <br/>
            </form>

            </div>
            </div>

             )
        }
    }


export default Lists;